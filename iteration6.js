// Iteración #6: Mixed For...of e includes
// Usa un bucle for...of para recorrer todos los juguetes y elimina los que incluyan la palabra gato.
// Recuerda que puedes usar la función .includes()

// RESUELTO CON AYUDA DE ALBERTO para que esté donde esté el objeto que contenga gato funcione 
const toys = [
    {id: 5, name: 'Buzz MyYear'}, 
    {id: 11, name: 'Action Woman'}, 
    {id: 23, name: 'Barbie Man'}, 
    {id: 40, name: 'El gato felix'},
    {id: 40, name: 'El gato con Guantes'},
  ]

const pos = [];
for(const toy of toys) {
	if(toy.name.includes('gato')) {
			pos.push(toys.indexOf(toy))
	}
}
for(let i = pos.length - 1; i >= 0; i--) {
	toys.splice(pos[i], 1);
}
console.log(toys)	

// yo he llegado hasta aquí sin ayuda de Juan pero no sabía como eliminar el objeto que cumple la condicion
/* const toys = [
    {id: 5, name: 'Buzz MyYear'}, 
    {id: 11, name: 'Action Woman'}, 
    {id: 23, name: 'Barbie Man'}, 
    {id: 40, name: 'El gato con Guantes'},
    {id: 40, name: 'El gato felix'}
    ]
for(var toy of toys) {
    if(!toy.name.includes('gato')) {
            console.log('no tengo gato');
    }
        if(toy.name.includes('gato')) {
            // console.log(toy.name + ' incluye gato');
            console.log(toy);
        }
}
 */