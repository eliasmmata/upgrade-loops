// **Iteración #5: Probando For**

// Usa un bucle **for** para recorrer todos los destinos del array
// y elimina los elementos que tengan el id 11 y 40.
// Imprime en un ***console log*** el array. Puedes usar este array:
const placesToTravel = [
    { id: 5, name: 'Japan' },
    { id: 11, name: 'Venecia' },
    { id: 23, name: 'Murcia' },
    { id: 40, name: 'Santander' },
    { id: 44, name: 'Filipinas' },
    { id: 59, name: 'Madagascar' }
]
const placesToTravelNew = [];


for(var i = 0, len = placesToTravel.length; i < len; i++) {
    if(placesToTravel[i].id !== 11 && placesToTravel[i].id !== 40){
        placesToTravelNew.push(placesToTravel[i]);
    };
};
console.log(placesToTravelNew);

// sin crear un nuevo Array vacío no he sido capaz de hacerlo, no ha habido manera... :(
